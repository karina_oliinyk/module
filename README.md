# MODULE

There are four files in folder src:

* index.js: contain import of files sum and pow
* require.js: contain module with the function require and realisation of this function
* sum.js: contain realisation of function sum and export this function
* pow.js: contain realisation of function pow and export this function

## Objective of the project:

* understand the module
* understand how the require to work
* implement own realisation of the require
